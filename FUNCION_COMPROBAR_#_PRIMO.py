
# autor: FREDDY YUNGA
# fecha: 22-05-2020

def detectar_primo(num):
    i=1
    pri=0
    while i<=num:
        if num % i ==0:
            pri=pri+1
        i=i+1
    if pri != 2 :
        print(" No es primo, porque es divisible para otros numeros")
    else:
        print(" Es primo, porque es divisible para si mismo y para 1")

if __name__=='__main__':
    numero=int(input(" Ingrese numero: "))
    detectar_primo(numero)
